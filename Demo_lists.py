print ('Test from Visual Studio.')

demo_list_1 = [3, 5, 3, 7, 8, 5, 5, 2]
demo_list_2 = [3, 0, 2, 1, 2]
demo_list_sorted = demo_list_2.sort
demo_set = set(demo_list_2)


gen_removed = (x for x in demo_list_2 if x not in demo_list_1)

list_removed = list(gen_removed)

for i in list_removed:
    print i